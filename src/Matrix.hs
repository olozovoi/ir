{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns  #-}
module Matrix where

import Data.Bits ((.|.), (.&.))
import Data.Maybe (maybeToList)
import qualified Data.Map as M
import Data.Binary
import Data.Vector.Binary
import qualified Data.Text as T
import qualified Data.Vector as V
import GHC.Generics

import Container
import Files as F

data Matrix = Matrix { _files :: V.Vector FilePath
                     , _rows  :: M.Map T.Text Integer
                     } deriving (Eq, Generic)

instance Container Matrix where
  empty = Matrix V.empty M.empty

instance Binary Matrix where

instance Show Matrix where
  show (Matrix fs index) = fs' ++ res where
    fs' = V.foldr (\f s -> f ++ ' ':s) "" fs
    res = M.foldrWithKey foldRow "" index
    foldRow w is res = T.unpack w ++ " : " ++ files is ++ '\n':res
    files = foldr (\f s -> f:' ':s) "" . intToList
    intToList k = foldr (\a acc -> if a.&.k /= 0 then '1':acc else '0':acc) [] [2^i | i <- [0..V.length fs - 1]]

insert :: Integer -> T.Text -> Matrix -> Matrix
insert _ (T.unpack -> "") m     = m
insert fi word (Matrix fs rows) = Matrix fs $ M.insertWith (.|.) word fi rows

lookup :: T.Text -> Matrix -> [FilePath]
lookup word (Matrix fs rows) = fs' where
  fi = maybeToList $ rows M.!? word
  is = [ \k -> (k.&.(2^i),i) | i <- [0..V.length fs - 1]] <*> fi
  fs' = map ((fs V.!) . snd) . filter ((/=0).fst) $ is

buildMatrix :: [FilePath] -> IO Matrix
buildMatrix fs = do
  let fsv = V.fromList fs
  words <- zip [ 2^i | let n = V.length fsv - 1, i <- [0..n]] <$> traverse F.words fs
  let matrix = foldr (\(fi, ws) m -> foldr (insert fi) m ws) (Matrix fsv M.empty) words
  return matrix