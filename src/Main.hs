{-# LANGUAGE BangPatterns #-}
module Main where

import System.Clock (getTime, Clock(..), TimeSpec(..))
import System.Directory
import System.FilePath
import System.IO

import  Data.Foldable (traverse_)
import Data.Map.Strict ((!?))
import Data.Text (pack)

import qualified Dict as D
import Files

main :: IO ()
main = traverse_ time [main1]

time :: IO a -> IO ()
time action = do
  putStrLn "Start"
  time0 <- getTime Realtime
  !_ <- action
  time1 <- getTime Realtime
  putStrLn $ "Elapsed time: " ++ show (sec $ time1 - time0) ++ "s"

main1 :: IO ()
main1 = files2 >>= D.buildDict >>= save' "dict.txt"

files1 = txtfiles "files"
files2 = txtfiles "files2"

txtfiles :: FilePath -> IO [FilePath]
txtfiles dir = map (dir </>) . filter (".txt" `isExtensionOf`) <$> getDirectoryContents dir


{-booleanSearch :: IO ()
booleanSearch = do
  itime0 <- getTime Realtime
  !index <- txtfiles "files" >>= I.buildIIndex
  itime1 <- getTime Realtime
  let ditime = itime1 - itime0
  putStrLn $ "index built in " ++ show (sec ditime) ++ "s" ++ show (nsec ditime) ++ "ns"
  loop index where
    loop index = do
      putStr "Query: "
      hFlush stdout
      q <- pack <$> getLine
      qtime1 <- getTime Realtime
      let res = runQuery index q
      either print print res
      qtime2 <- getTime Realtime
      let dqtime = qtime2 - qtime1
      putStrLn $ "query took " ++ show (sec dqtime) ++ "s" ++ show (nsec dqtime) ++ "ns"
      loop index-}