module Tree where

import qualified Data.Map.Strict as M
import Data.Set
import Data.Text (Text)

import Container
import qualified Dict as D
import Files as F

instance Container (Set a) where
  empty = Data.Set.empty

buildTree :: [FilePath] -> IO (Set Text)
buildTree fs = Prelude.foldr insert Data.Set.empty . concat <$> traverse F.words fs

fromDict :: D.Dict -> Set Text
fromDict (D.Dict dict _) = M.keysSet dict