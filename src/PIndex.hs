{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TupleSections #-}
module PIndex where

import Control.Monad (forM)
import Data.Coerce
import qualified Data.IntSet as S
import qualified Data.Map.Strict as M
import Data.Maybe (maybeToList)
import Data.Binary
import Data.Vector.Binary
import qualified Data.Text as T
import qualified Data.Vector as V
import GHC.Base (sconcat, NonEmpty(..))
import GHC.Generics

import Container
import IIndex
import qualified Files as F

data PIndex = PIndex { _files :: V.Vector FilePath
                     , _index :: M.Map (T.Text, T.Text) S.IntSet }
                     deriving (Eq, Generic, Show)

instance Container PIndex where
  empty = PIndex V.empty M.empty

instance Binary PIndex where

buildPIndex :: [FilePath] -> IO PIndex
buildPIndex fs = (PIndex $ V.fromList fs) . M.fromList
  . concatMap (\(i, ws) -> (,i) <$> F.phrases ws) 
  . zip (S.singleton <$> [0..])
  <$> traverse F.words fs

phraseSearch :: T.Text -> PIndex -> [FilePath]
phraseSearch sentence (PIndex fv ws) = fs where
  ids = concat . maybeToList . fmap (S.toList . S.unions) $ traverse (ws M.!?) (F.phrases $ T.words sentence)
  fs = map (fv V.!) ids