{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TupleSections #-}
module CIndex where

import qualified Data.IntSet as S
import qualified Data.Map.Strict as M
import Data.Map.Merge.Strict
import Data.Binary
import Data.Vector.Binary
import qualified Data.Text as T
import qualified Data.Vector as V
import GHC.Generics

import Container
import qualified Files as F

data CIndex = CIndex { _files :: V.Vector FilePath
                     , _index :: M.Map T.Text (M.Map Int S.IntSet) }
                     deriving (Eq, Generic)

instance Binary CIndex where

instance Container CIndex where
  empty = CIndex V.empty M.empty

insertWords :: Int -> [(Int, T.Text)] -> CIndex -> CIndex
insertWords fi ws (CIndex fs index) = CIndex fs $ foldr foldrWs M.empty ws where
  foldrWs (i, word) = M.insertWith helper word (M.singleton fi $ S.singleton i)
  helper = merge preserveMissing preserveMissing (zipWithMatched $ const $ S.union)


buildCIndex :: [FilePath] -> IO CIndex
buildCIndex fs = foldr (\(fi, ws) ci -> insertWords fi ws ci) (CIndex (V.fromList fs) M.empty) 
  . zip [0..] 
  . map (zip [0..]) 
  <$> traverse F.words fs