{-# LANGUAGE ScopedTypeVariables #-}
module Files where

import Codec.Text.Detect
import Control.Exception
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import Data.Char (isAlpha)
import qualified Data.Text.Lazy as T
import qualified Data.Text.Encoding as E
import qualified Data.Text.ICU.Convert as C
import Data.Binary
import System.IO

import Container

save :: Binary a => FilePath -> a -> IO ()
save file a = encodeFile file a `catch` handler ()

save' :: Show a => FilePath -> a -> IO ()
save' file a = withFile file WriteMode (flip hPutStr $ show a) `catch` handler ()

load :: (Binary a, Container a) => FilePath -> IO a
load file = decodeFile file `catch` handler empty

lexem :: T.Text -> T.Text
lexem = strip . T.filter (\c -> isAlpha c || c `elem` "'-") . T.toLower

strip :: T.Text -> T.Text
strip = T.dropAround (not . isAlpha)

lexems :: T.Text -> [T.Text]
lexems = filter (not . T.null) . concatMap (T.splitOn $ T.pack "-") . map lexem . T.words

words :: FilePath -> IO [T.Text]
words file = withFile file ReadMode (\h -> do
  maybeChSet <- detectEncodingName <$> BL.readFile file
  maybe failed ok maybeChSet) `catch` handler []
    where
      failed = putStrLn ("Cannot detect encoding of file '" ++ file ++ "'") >> return []
      ok chSet = do
        conv <- C.open chSet Nothing
        lexems . T.fromStrict . C.toUnicode conv <$> withFile file ReadMode B.hGetContents


handler :: a -> SomeException -> IO a
handler a e = print e >> return a

phrases :: [T.Text] -> [(T.Text, T.Text)]
phrases ts = zip ts (tail ts)

permutate :: T.Text -> [T.Text]
permutate word = go (T.length word) word where
  next word = let Just (c, cs) = T.uncons word in T.snoc cs c
  go 0 _ = []
  go n word = word : go (n-1) (next word)

permutate' :: T.Text -> [T.Text]
permutate' word = permutate $ T.snoc word '$'
