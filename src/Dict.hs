{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
module Dict where

import Control.DeepSeq
import qualified Data.Set as S
import Data.Binary
import qualified Data.Text.Lazy as T
import GHC.Generics

import Container
import Files as F

data Dict = Dict { _dict :: S.Set T.Text, _size :: Int } deriving (Eq, Generic)

instance Binary Dict where
instance NFData Dict where

instance Container Dict where
  empty = Dict S.empty 0

instance Show Dict where
  show Dict{..} = "Total words: " ++ show total ++ "\nUnique words: " ++ show unique ++ "\n\nDictionary:\n" ++ wordsOcc where
    total = _size
    unique = S.size _dict
    wordsOcc = S.foldr (\w s -> T.unpack w ++ '\n':s) "" _dict

insert :: T.Text -> Dict -> Dict
insert (T.unpack -> "") dict = dict
insert lexem Dict{..} = Dict (S.insert lexem _dict) (succ _size)

dictFromWords :: [T.Text] -> Dict
dictFromWords = dictFromWords' empty

dictFromWords' :: Dict -> [T.Text] -> Dict
dictFromWords' = foldr insert

buildDict :: [FilePath] -> IO Dict
buildDict fs = dictFromWords . concat <$> traverse F.words fs