module BoolSearch (runQuery) where

import qualified Data.IntSet as S
import Data.Map ((!?))
import qualified Data.Text as T
import qualified Data.Vector as V
import Data.Void
import Text.Parsec
import Text.Parsec.Text

import Files (lexem)
import IIndex

data Query = Word T.Text
           | Not Query
           | And Query Query
           deriving (Show, Eq)

evalQuery :: IIndex -> Query -> S.IntSet
evalQuery index (Word word)             = maybe S.empty id $ _index index !? lexem word
evalQuery index (Not query)             = S.fromList [0..V.length (_files index) - 1] S.\\ evalQuery index query
evalQuery index (And (Not q1) (Not q2)) = evalQuery index (Not $ And q1 q2)
evalQuery index (And q1 (Not q2))       = evalQuery index q1 S.\\ evalQuery index q2
evalQuery index (And (Not q1) q2)       = evalQuery index q2 S.\\ evalQuery index q1
evalQuery index (And q1 q2)             = evalQuery index q1 `S.union` evalQuery index q2

runQuery :: IIndex -> T.Text -> Either ParseError [FilePath]
runQuery index queryT = fs where
  query = runParser queryP () "query" queryT
  is = evalQuery index <$> query
  fs = map (_files index V.!) . S.toList <$> is

opAnd :: Parser (Query -> Query -> Query)
opAnd = char '&' *> spaces *> pure And

opNot :: Parser (Query -> Query)
opNot = char '!' *> spaces *> pure Not

word :: Parser Query
word = Word . lexem . T.pack <$> between (char '"') (char '"') (many1 $ noneOf "\"")

factor :: Parser Query
factor = between (char '(' <* spaces) (char ')' <* spaces) expr <|> word <* spaces

term :: Parser Query
term = option id opNot <*> factor

expr :: Parser Query
expr = chainl1 term opAnd

queryP :: Parser Query
queryP = spaces *> expr <* eof

