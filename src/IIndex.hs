{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TupleSections #-}
module IIndex(
  IIndex(..)
  ,buildIIndex
  ,queryWord
  ) where

import Control.DeepSeq
import Control.Monad (forM)
import qualified Data.IntSet as S
import Data.Map.Merge.Strict
import qualified Data.Map.Strict as M
import Data.Binary
import Data.Vector.Binary
import qualified Data.Text.Lazy as T
import qualified Data.Vector as V
import GHC.Base (sconcat, NonEmpty(..))
import GHC.Generics

import Container
import Files as F

data IIndex = IIndex { _files :: V.Vector FilePath
                     , _index :: M.Map T.Text S.IntSet }
                     deriving (Eq, Generic)

instance Container IIndex where
  empty = IIndex V.empty M.empty

instance Show IIndex where
  show (IIndex fs index) = res where
    res = M.foldrWithKey foldRow "" index
    foldRow w is res = T.unpack w ++ " : " ++ files is ++ '\n':res
    files = foldr (\f s -> f ++ ' ':s) "" . map (fs V.!) . S.toList

instance NFData IIndex where
instance Binary IIndex where
  
buildIIndex :: [FilePath] -> IO IIndex
buildIIndex fs = (IIndex $ V.fromList fs) . M.fromListWith S.union
  . concatMap (\(i,ws) -> (,i) <$> ws)
  . zip (S.singleton <$> [0..])
  <$> traverse F.words fs

queryWord :: String -> IIndex -> [FilePath]
queryWord word index = fs where
    word' = T.pack word
    fsIdSet = _index index M.!? word'
    fs = maybe [] (map (_files index V.!) . S.toList) fsIdSet
