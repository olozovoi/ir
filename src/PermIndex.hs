{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TupleSections #-}
module PermIndex where

import Data.Binary
import qualified Data.IntSet as S
import Data.List (elemIndices, splitAt)
import Data.Maybe (maybeToList)
import qualified Data.Map.Strict as M
import qualified Data.Text as T
import qualified Data.Vector as V
import GHC.Generics

import Container
import IIndex
import Files

data PermIndex = PermIndex { _files :: V.Vector FilePath
                           , _index :: M.Map T.Text S.IntSet }
                           deriving (Eq, Generic, Show)

instance Binary PermIndex where

instance Container PermIndex where
  empty = PermIndex V.empty M.empty

buildPermIndex :: [FilePath] -> IO PermIndex
buildPermIndex fs = (PermIndex $ V.fromList fs) . M.fromList
  . concatMap (\(i, ws) -> (,i) <$> concatMap Files.permutate' ws)
  . zip (S.singleton <$> [0..])
  <$> traverse Files.words fs

fromIIndex :: IIndex -> PermIndex
fromIIndex (IIndex fs index) = PermIndex fs
  . M.fromList
  . concatMap (\(w,s) -> (,s) <$> Files.permutate' w)
  . M.toList $ index

search :: String -> PermIndex -> [FilePath]
search query (PermIndex fs index)
  | '*' `notElem` query = []
  | otherwise = res where
    (jokerInd:_) = elemIndices '*' query
    (s1, (_:s2)) = splitAt jokerInd query
    query' = T.pack $ s2 ++ '$':s1
    ids = S.toList . S.unions . map snd . filter (T.isPrefixOf  query' . fst) $ M.toList index
    res = map (fs V.!) ids