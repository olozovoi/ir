#!/usr/bin/env bash
cabal2nix --shell ./. > shell.nix
sed 's/pkgs.haskellPackages/pkgs.profiledHaskellPackages/' shell.nix > profiling-shell.nix