{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, async, base, binary, bytestring
      , charsetdetect-ae, clock, containers, deepseq, directory, filepath
      , haskey-btree, parsec, stdenv, text, text-icu, vector
      , vector-binary-instances
      }:
      mkDerivation {
        pname = "ir";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = false;
        isExecutable = true;
        executableHaskellDepends = [
          async base binary bytestring charsetdetect-ae clock containers
          deepseq directory filepath haskey-btree parsec text text-icu vector
          vector-binary-instances
        ];
        license = stdenv.lib.licenses.bsd3;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
